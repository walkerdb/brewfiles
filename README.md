# Brewfile

Repo holding a brew file to add all of my day-to-day tools, meant to help set up new machines.

#### Usage
```bash
./set-up-new-machine.sh
```
