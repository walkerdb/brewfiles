# set up rbenv
eval "$(rbenv init -)"

# set up go
export GOPATH="$HOME/go"
export PATH="$PATH:${GOPATH//://bin:}/bin"

# set up postgres
export PATH="/usr/local/opt/postgresql@10/bin:$PATH"

# oh-my-zsh config. See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes for more themes
export ZSH="${HOME}/.oh-my-zsh"

ZSH_THEME=""
COMPLETION_WAITING_DOTS="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"
HIST_STAMPS="yyyy-mm-dd"
plugins=(
  adb
  brew
  docker
  gradle
  nvm
  pod
  react-native
  yarn

  # manually installed plugins:
  zsh-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh

# set up theme
autoload -U promptinit; promptinit
prompt pure