#!/usr/bin/env bash

# install homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew bundle check
brew bundle install

# oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting"
npm install --global pure-prompt

mv ~/.zshrc ~/.zshrc.old
cp .zshrc ~/.zshrc
source ~/.zshrc

nvm install 16
nvm default 16

curl https://raw.githubusercontent.com/sindresorhus/iterm2-snazzy/main/Snazzy.itermcolors > Snazzy.itermcolors
open "Snazzy.itermcolors"

open "/Applications/iTerm.app"
